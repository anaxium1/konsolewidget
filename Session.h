#ifndef SESSION_H
#define SESSION_H

// Qt
#include <QStringList>
#include <QHash>
#include <QUuid>
#include <QSize>
#include <QProcess>
#include <QWidget>
#include <QUrl>

class QColor;

namespace Konsole {
class Emulation;
class Pty;
class ProcessInfo;
class TerminalDisplay;
class HistoryType;

/**
 * Represents a terminal session consisting of a pseudo-teletype and a terminal emulation.
 * The pseudo-teletype (or PTY) handles I/O between the terminal process and Konsole.
 * The terminal emulation ( Emulation and subclasses ) processes the output stream from the
 * PTY and produces a character image which is then shown on views connected to the session.
 *
 * Each Session can be connected to one or more views by using the addView() method.
 * The attached views can then display output from the program running in the terminal
 * or send input to the program in the terminal in the form of keypresses and mouse
 * activity.
 */
class Session : public QObject
{
    Q_OBJECT

public:
    using Ptr = QPointer<Session>;

    Q_PROPERTY(QString keyBindings READ keyBindings WRITE setKeyBindings)
    Q_PROPERTY(QSize size READ size WRITE setSize)

    /**
     * Constructs a new session.
     *
     * To start the terminal process, call the run() method,
     * after specifying the program and arguments
     * using setProgram() and setArguments()
     *
     * If no program or arguments are specified explicitly, the Session
     * falls back to using the program specified in the SHELL environment
     * variable.
     */
    explicit Session(QObject *parent = nullptr);
    ~Session() override;

    /**
     * Returns true if the tab holding this session is currently selected
     * and Konsole is the foreground window.
     */
    bool hasFocus() const;

    /**
     * Adds a new view for this session.
     *
     * The viewing widget will display the output from the terminal and
     * input from the viewing widget (key presses, mouse activity etc.)
     * will be sent to the terminal.
     *
     * Views can be removed using removeView().  The session is automatically
     * closed when the last view is removed.
     */
    void addView(TerminalDisplay *widget);
    /**
     * Removes a view from this session.  When the last view is removed,
     * the session will be closed automatically.
     *
     * @p widget will no longer display output from or send input
     * to the terminal
     */
    void removeView(TerminalDisplay *widget);

    /**
     * Returns the views connected to this session
     */
    QList<TerminalDisplay *> views() const;

    /**
     * Returns the terminal emulation instance being used to encode / decode
     * characters to / from the process.
     */
    Emulation *emulation() const;

    /**
     * Sets the type of history store used by this session.
     * Lines of output produced by the terminal are added
     * to the history store.  The type of history store
     * used affects the number of lines which can be
     * remembered before they are lost and the storage
     * (in memory, on-disk etc.) used.
     */
    void setHistoryType(const HistoryType &hType);
    /**
     * Returns the type of history store used by this session.
     */
    const HistoryType &historyType() const;
    /**
     * Clears the history store used by this session.
     */
    void clearHistory();

    /**
     * Sets the key bindings used by this session.  The bindings
     * specify how input key sequences are translated into
     * the character stream which is sent to the terminal.
     *
     * @param name The name of the key bindings to use.  The
     * names of available key bindings can be determined using the
     * KeyboardTranslatorManager class.
     */
    void setKeyBindings(const QString &name);
    /** Returns the name of the key bindings used by this session. */
    QString keyBindings() const;

    /** Returns the terminal session's window size in lines and columns. */
    QSize size();
    /**
     * Emits a request to resize the session to accommodate
     * the specified window size.
     *
     * @param size The size in lines and columns to request.
     */
    void setSize(const QSize &size);

    QSize preferredSize() const;

    void setPreferredSize(const QSize &size);

    /**
     * Sets whether the session has a dark background or not.  The session
     * uses this information to set the COLORFGBG variable in the process's
     * environment, which allows the programs running in the terminal to determine
     * whether the background is light or dark and use appropriate colors by default.
     *
     * This has no effect once the session is running.
     */
    void setDarkBackground(bool darkBackground);

    /**
      * Possible values of the @p what parameter for setSessionAttribute().
      * See the "Operating System Commands" section at:
      * https://invisible-island.net/xterm/ctlseqs/ctlseqs.html#h3-Operating-System-Commands
      */
    enum SessionAttributes {
        IconNameAndWindowTitle = 0,
        IconName = 1,
        WindowTitle = 2,
        CurrentDirectory = 7,         // From VTE (supposedly 6 was for dir, 7 for file, but whatever)
        TextColor = 10,
        BackgroundColor = 11,
        SessionName = 30,             // Non-standard
        SessionIcon = 32,             // Non-standard
        ProfileChange = 50            // this clashes with Xterm's font change command
    };

    // Sets the text codec used by this sessions terminal emulation.
    void setCodec(QTextCodec *codec);

    void reportColor(SessionAttributes r, const QColor& c, uint terminator);
    void reportForegroundColor(const QColor &c, uint terminator);
    void reportBackgroundColor(const QColor &c, uint terminator);

    bool isReadOnly() const;
    void setReadOnly(bool readOnly);

public Q_SLOTS:

    void onReceiveBlock(const char *buf, int len);

    /**
     * @param text to send to the current foreground terminal program.
     * @param eol send this after @p text
     */
    void sendTextToTerminal(const QString &text, const QChar &eol = QChar()) const;

    /**
     * Sends a mouse event of type @p eventType emitted by button
     * @p buttons on @p column/@p line to the current foreground
     * terminal program
     */
    void sendMouseEvent(int buttons, int column, int line, int eventType);

    /** Sets the text codec used by this sessions terminal emulation.
      * Overloaded to accept a QByteArray for convenience since DBus
      * does not accept QTextCodec directly.
      */
    bool setCodec(const QByteArray &name);

    /** Returns the codec used to decode incoming characters in this
     * terminal emulation
     */
    QByteArray codec();

    /**
     * Sets the history capacity of this session.
     *
     * @param lines The history capacity in unit of lines. Its value can be:
     * <ul>
     * <li> positive integer  -  fixed size history</li>
     * <li> 0 -  no history</li>
     * <li> negative integer -  unlimited history</li>
     * </ul>
     */
    void setHistorySize(int lines);

    /**
     * Returns the history capacity of this session.
     */
    int historySize() const;

Q_SIGNALS:

    /** Emitted when the terminal process starts. */
    void started();

    /**
     * Emitted when the terminal process exits.
     */
    void finished();

    /**
     * Emitted when one of certain session attributes has been changed.
     * See setSessionAttribute().
     */
    void sessionAttributeChanged();

    /** Emitted when the session gets locked / unlocked. */
    void readOnlyChanged();

    /**
     * Emitted when the session text encoding changes.
     */
    void sessionCodecChanged(QTextCodec *codec);

    /**
     * Requests that the background color of views on this session
     * should be changed.
     */
    void changeBackgroundColorRequest(const QColor &);
    /**
     * Requests that the text color of views on this session should
     * be changed to @p color.
     */
    void changeForegroundColorRequest(const QColor &);

    /**
     * Emitted when the terminal process requests a change
     * in the size of the terminal window.
     *
     * @param size The requested window size in terms of lines and columns.
     */
    void resizeRequest(const QSize &size);

    /**
     * Emitted when a profile change command is received from the terminal.
     *
     * @param text The text of the command.  This is a string of the form
     * "PropertyName=Value;PropertyName=Value ..."
     */
    void profileChangeCommandReceived(const QString &text);

    /**
     * Emitted when the text selection is changed.
     *
     * This signal serves as a relayer of Emulation::selectedText(QString),
     * making it usable for higher level component.
     */
    void selectionChanged(const QString &text);

    /**
     * Emitted when foreground request ("\033]10;?\a") terminal code received.
     * Terminal is expected send "\033]10;rgb:RRRR/GGGG/BBBB\a" response.
     */
    void getForegroundColor(uint terminator);

    /**
     * Emitted when background request ("\033]11;?\a") terminal code received.
     * Terminal is expected send "\033]11;rgb:RRRR/GGGG/BBBB\a" response.
     *
     * Originally implemented to support vim's background detection feature
     * (without explicitly setting 'bg=dark' within local/remote vimrc)
     */
    void getBackgroundColor(uint terminator);

private Q_SLOTS:
    void onViewSizeChange(int height, int width);

private:
    Q_DISABLE_COPY(Session)

    void updateTerminalSize();

    Emulation *_emulation;

    QList<TerminalDisplay *> _views;

    bool _hasDarkBackground;

    QSize _preferredSize;

    bool _readOnly;
};

}

#endif
