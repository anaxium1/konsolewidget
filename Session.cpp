// Own
#include "Session.h"

// Standard
#include <cstdlib>

// Qt
#include <QApplication>
#include <QColor>
#include <QDir>
#include <QFile>
#include <QKeyEvent>

// Konsole
#include "Vt102Emulation.h"
#include "TerminalDisplay.h"
#include "CompactHistoryType.h"

using namespace Konsole;

Session::Session(QObject* parent) :
    QObject(parent)
    , _emulation(nullptr)
    , _views(QList<TerminalDisplay *>())
    , _hasDarkBackground(false)
    , _preferredSize(QSize())
    , _readOnly(false)
{
    //create emulation backend
    _emulation = new Vt102Emulation();
    _emulation->reset();

    connect(_emulation, &Konsole::Emulation::profileChangeCommandReceived, this, &Konsole::Session::profileChangeCommandReceived);
    connect(_emulation, &Konsole::Emulation::selectionChanged, this, &Konsole::Session::selectionChanged);
    connect(_emulation, &Konsole::Emulation::imageResizeRequest, this, &Konsole::Session::resizeRequest);
}

Session::~Session()
{
    delete _emulation;
}

void Session::setDarkBackground(bool darkBackground)
{
    _hasDarkBackground = darkBackground;
}

bool Session::hasFocus() const
{
    return std::any_of(_views.constBegin(), _views.constEnd(),
                      [](const TerminalDisplay *display) { return display-> hasFocus(); });
}

void Session::setCodec(QTextCodec* codec)
{
    if (isReadOnly()) {
        return;
    }

    emulation()->setCodec(codec);

    Q_EMIT sessionCodecChanged(codec);
}

bool Session::setCodec(const QByteArray& name)
{
    QTextCodec* codec = QTextCodec::codecForName(name);

    if (codec != nullptr) {
        setCodec(codec);
        return true;
    } else {
        return false;
    }
}

QByteArray Session::codec()
{
    return _emulation->codec()->name();
}

QList<TerminalDisplay*> Session::views() const
{
    return _views;
}

void Session::addView(TerminalDisplay* widget)
{
    Q_ASSERT(!_views.contains(widget));

    _views.append(widget);

    // connect emulation - view signals and slots
    connect(widget, &Konsole::TerminalDisplay::keyPressedSignal, _emulation, &Konsole::Emulation::sendKeyEvent);
    connect(widget, &Konsole::TerminalDisplay::mouseSignal, _emulation, &Konsole::Emulation::sendMouseEvent);
    connect(widget, &Konsole::TerminalDisplay::sendStringToEmu, _emulation, &Konsole::Emulation::sendString);
    connect(widget, &Konsole::TerminalDisplay::peekPrimaryRequested, _emulation, &Konsole::Emulation::setPeekPrimary);

    // allow emulation to notify the view when the foreground process
    // indicates whether or not it is interested in Mouse Tracking events
    connect(_emulation, &Konsole::Emulation::programRequestsMouseTracking, widget, &Konsole::TerminalDisplay::setUsesMouseTracking);

    widget->setUsesMouseTracking(_emulation->programUsesMouseTracking());

    // TODO: Is this required?
    //connect(_emulation, &Konsole::Emulation::enableAlternateScrolling, widget->scrollBar(), &Konsole::TerminalScrollBar::setAlternateScrolling);

    connect(_emulation, &Konsole::Emulation::programBracketedPasteModeChanged, widget, &Konsole::TerminalDisplay::setBracketedPasteMode);

    widget->setBracketedPasteMode(_emulation->programBracketedPasteMode());

    widget->setScreenWindow(_emulation->createWindow());

    _emulation->setCurrentTerminalDisplay(widget);

    connect(widget, &Konsole::TerminalDisplay::changedContentSizeSignal, this, &Konsole::Session::onViewSizeChange);

    connect(widget, &Konsole::TerminalDisplay::compositeFocusChanged, _emulation, &Konsole::Emulation::focusChanged);

    connect(_emulation, &Konsole::Emulation::setCursorStyleRequest, widget, &Konsole::TerminalDisplay::setCursorStyle);
}

void Session::onViewSizeChange(int /*height*/, int /*width*/)
{
    updateTerminalSize();
}

void Session::updateTerminalSize()
{
    int minLines = -1;
    int minColumns = -1;

    // minimum number of lines and columns that views require for
    // their size to be taken into consideration ( to avoid problems
    // with new view widgets which haven't yet been set to their correct size )
    const int VIEW_LINES_THRESHOLD = 2;
    const int VIEW_COLUMNS_THRESHOLD = 2;

    //select largest number of lines and columns that will fit in all visible views
    for (TerminalDisplay *view : qAsConst(_views)) {
        if ( !view->isHidden() &&
                view->lines() >= VIEW_LINES_THRESHOLD &&
                view->columns() >= VIEW_COLUMNS_THRESHOLD) {
            minLines = (minLines == -1) ? view->lines() : qMin(minLines , view->lines());
            minColumns = (minColumns == -1) ? view->columns() : qMin(minColumns , view->columns());
            view->processFilters();
        }
    }

    // backend emulation must have a _terminal of at least 1 column x 1 line in size
    if (minLines > 0 && minColumns > 0) {
        _emulation->setImageSize(minLines , minColumns);
    }
}

void Session::reportColor(SessionAttributes r, const QColor& c, uint terminator)
{
    #define to65k(a) (QStringLiteral("%1").arg(int(((a)*0xFFFF)), 4, 16, QLatin1Char('0')))
    QString msg = QStringLiteral("\033]%1;rgb:").arg(r)
                + to65k(c.redF())   + QLatin1Char('/')
                + to65k(c.greenF()) + QLatin1Char('/')
                + to65k(c.blueF());

    // Match termination of OSC reply to termination of OSC request.
    if (terminator == '\a') { // non standard BEL terminator
        msg += QLatin1Char('\a');
    } else { // standard 7-bit ST terminator
        msg += QStringLiteral("\033\\");
    }
    _emulation->sendString(msg.toUtf8());
    #undef to65k
}

void Session::reportForegroundColor(const QColor& c, uint terminator)
{
    reportColor(SessionAttributes::TextColor, c, terminator);
}

void Session::reportBackgroundColor(const QColor& c, uint terminator)
{
    reportColor(SessionAttributes::BackgroundColor, c, terminator);
}

void Session::sendTextToTerminal(const QString& text, const QChar& eol) const
{
    if (isReadOnly()) {
        return;
    }

    if (eol.isNull()) {
        _emulation->sendText(text);
    } else {
        _emulation->sendText(text + eol);
    }
}

void Session::sendMouseEvent(int buttons, int column, int line, int eventType)
{
    if (isReadOnly()) {
        return;
    }

    _emulation->sendMouseEvent(buttons, column, line, eventType);
}

Emulation* Session::emulation() const
{
    return _emulation;
}

QString Session::keyBindings() const
{
    return _emulation->keyBindings();
}

void Session::setKeyBindings(const QString& name)
{
    _emulation->setKeyBindings(name);
}

void Session::setHistoryType(const HistoryType& hType)
{
    _emulation->setHistory(hType);
}

const HistoryType& Session::historyType() const
{
    return _emulation->history();
}

void Session::clearHistory()
{
    _emulation->clearHistory();
}

void Session::onReceiveBlock(const char* buf, int len)
{
    _emulation->receiveData(buf, len);
}

QSize Session::size()
{
    return _emulation->imageSize();
}

void Session::setSize(const QSize& size)
{
    if ((size.width() <= 1) || (size.height() <= 1)) {
        return;
    }

    Q_EMIT resizeRequest(size);
}

QSize Session::preferredSize() const
{
    return _preferredSize;
}

void Session::setPreferredSize(const QSize& size)
{
    _preferredSize = size;
}

void Session::setHistorySize(int lines)
{
    //if (isReadOnly()) {
    //    return;
    //}

    // TODO: Maybe support other history types
    /*
    if (lines < 0) {
        setHistoryType(HistoryTypeFile());
    } else if (lines == 0) {
        setHistoryType(HistoryTypeNone());
    } else {
        setHistoryType(CompactHistoryType(lines));
    }
    */

    setHistoryType(CompactHistoryType(lines));
}

int Session::historySize() const
{
    const HistoryType& currentHistory = historyType();

    if (currentHistory.isEnabled()) {
        if (currentHistory.isUnlimited()) {
            return -1;
        } else {
            return currentHistory.maximumLineCount();
        }
    } else {
        return 0;
    }
}

bool Session::isReadOnly() const
{
    return _readOnly;
}

void Session::setReadOnly(bool readOnly)
{
    if (_readOnly != readOnly) {
        _readOnly = readOnly;

        // Needed to update the tab icons and all
        // attached views.
        Q_EMIT readOnlyChanged();
    }
}
