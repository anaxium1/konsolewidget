#ifndef RANDOMIZATIONRANGE_H
#define RANDOMIZATIONRANGE_H

namespace Konsole
{

    // specifies how much a particular color can be randomized by
    class RandomizationRange
    {
    public:
        RandomizationRange();
        bool isNull() const;

        double hue;
        double saturation;
        double lightness;
    };

}

#endif
