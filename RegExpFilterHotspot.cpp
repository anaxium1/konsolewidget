#include "RegExpFilterHotspot.h"

using namespace Konsole;


RegExpFilterHotSpot::RegExpFilterHotSpot(
    int startLine,
    int startColumn,
    int endLine,
    int endColumn,
    const QStringList &capturedTexts) :
    HotSpot(startLine, startColumn, endLine, endColumn),
    _capturedTexts(capturedTexts)
{
    setType(Marker);
}

void RegExpFilterHotSpot::activate(QObject *)
{
}

QStringList RegExpFilterHotSpot::capturedTexts() const
{
    return _capturedTexts;
}
