from PyQt5 import QtWidgets, QtGui
from PyQt5.QtWidgets import QWidget, QHBoxLayout, QVBoxLayout, QScrollBar, QLineEdit, QPushButton
from PyQt5.QtCore import QRegExp, Qt, QByteArray, QUrl, pyqtSlot
from PyQt5.QtGui import QDesktopServices, QColor
from KonsoleWidget import Konsole
import os
from colorama import Fore, Back, Style

colorTable = [
    QColor("#708284"), # foreground
    QColor("#001e27"), # background
    QColor("#002831"), # Black
    QColor("#d11c24"), # Red
    QColor("#738a05"), # Green
    QColor("#a57706"), # Yellow
    QColor("#2176c7"), # Blue
    QColor("#c61c6f"), # Magenta
    QColor("#259286"), # Cyan
    QColor("#eae3cb"), # White
    QColor("#708284"), # foregroundIntense
    QColor("#001e27"), # backgroundIntense
    QColor("#475b62"), # Black
    QColor("#bd3613"), # Red
    QColor("#475b62"), # Green
    QColor("#536870"), # Yellow
    QColor("#708284"), # Blue
    QColor("#5956ba"), # Magenta
    QColor("#819090"), # Cyan
    QColor("#fcf4dc"), # White
    QColor("#708284"), # foregroundFeint
    QColor("#001e27"), # backgroundFeint
    QColor("#002831"), # Black
    QColor("#d11c24"), # Red
    QColor("#738a05"), # Green
    QColor("#a57706"), # Yellow
    QColor("#2176c7"), # Blue
    QColor("#c61c6f"), # Magenta
    QColor("#259286"), # Cyan
    QColor("#eae3cb"), # White
]


class MainWindow(QWidget):
    def __init__(self):
        super().__init__()
        
        QDesktopServices.setUrlHandler("file", self.handleFileUrl)
        
        self.layout0 = QVBoxLayout(self)

        self.textbox = QLineEdit()
        self.button = QPushButton('Search')
        self.button.clicked.connect(self.buttonClicked)
        self.button2 = QPushButton('Add Text')
        self.button2.clicked.connect(self.addTextClicked)
        self.layout1 = QHBoxLayout(self)
        self.layout1.addWidget(self.textbox)
        self.layout1.addWidget(self.button)
        self.layout1.addWidget(self.button2)
        self.layout0.addLayout(self.layout1)
        
        self.scrollbar = QScrollBar()
        self.konsole = Konsole.KonsoleWidget()
        self.konsole.setExternalScrollBar(self.scrollbar)
        self.konsole.setAllowedLinkSchema(["file://"])
        self.konsole.setColorTable(colorTable)
        self.layout2 = QHBoxLayout(self)
        self.layout2.addWidget(self.konsole)
        self.layout2.addWidget(self.scrollbar)
        self.layout0.addLayout(self.layout2)
        
        self.setLayout(self.layout0)
        self.show()
    
    def addTextClicked(self):
        data = Style.RESET_ALL + 'hello world! hello world! hello world! hello world! hello world! hello world! hello world! hello world! \r\n'
        self.konsole.outputBlock(QByteArray(data.encode('utf-8')))
        
        data = Fore.RED + 'good bye!\r\n'
        self.konsole.outputBlock(QByteArray(data.encode('utf-8')))
        
        data = "\r\n\x1b]8;;{}\x1b\\This is a link\x1b]8;;\x1b\\\r\n".format(QUrl.fromLocalFile("C:\\test.txt").toString())
        self.konsole.outputBlock(QByteArray(data.encode('utf-8')))
    
    def buttonClicked(self):
        self.konsole.search(self.textbox.text(), False, False, False);

    @pyqtSlot(QUrl)
    def handleFileUrl(self, url):
        print(url.toLocalFile())


app = QtWidgets.QApplication([])
window = MainWindow()
app.exec()
