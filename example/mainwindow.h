#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QScrollBar>
#include "KonsoleWidget.h"

using namespace Konsole;

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);

private:
    QVBoxLayout* layout;
    QHBoxLayout* layout2;
    QPushButton* buttonAddData;
    QPushButton* buttonAddLink;
    QPushButton* buttonSearch;
    QPushButton* buttonScrollToEnd;
    QScrollBar* scrollBar;
    KonsoleWidget* konsole;

public slots:
    void clickedAddData();
    void clickedAddLink();
    void clickedSearch();
    void clickedScrollToEnd();
    void handleFileUrl(const QUrl &url);

signals:
    void onReceiveBlock(const char *buf, int len);

};

#endif

