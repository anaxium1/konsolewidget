#include "mainwindow.h"
#include "Session.h"
#include "TerminalDisplay.h"
#include <QTextCodec>
#include <QInputDialog>
#include <QMessageBox>
#include <QDesktopServices>

using namespace Konsole;

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
    buttonAddData = new QPushButton("Add Data", this);
    buttonAddLink = new QPushButton("Add Link", this);
    buttonSearch = new QPushButton("Search", this);
    buttonScrollToEnd = new QPushButton("Scroll to End", this);

    scrollBar = new QScrollBar(this);

    connect(buttonAddData, &QPushButton::clicked, this, &MainWindow::clickedAddData);
    connect(buttonAddLink, &QPushButton::clicked, this, &MainWindow::clickedAddLink);
    connect(buttonSearch, &QPushButton::clicked, this, &MainWindow::clickedSearch);
    connect(buttonScrollToEnd, &QPushButton::clicked, this, &MainWindow::clickedScrollToEnd);

    konsole = new KonsoleWidget(this);
    konsole->setAllowedLinkSchema(QStringList({ "file://" }));
    konsole->setExternalScrollBar(scrollBar);

    QVector<QColor> colorTable = {
        QColor("#708284"), // foreground
        QColor("#001e27"), // background
        QColor("#002831"), // Black
        QColor("#d11c24"), // Red
        QColor("#738a05"), // Green
        QColor("#a57706"), // Yellow
        QColor("#2176c7"), // Blue
        QColor("#c61c6f"), // Magenta
        QColor("#259286"), // Cyan
        QColor("#eae3cb"), // White

        QColor("#708284"), // FgIntense
        QColor("#001e27"), // BgIntense
        QColor("#475b62"), // Black
        QColor("#bd3613"), // Red
        QColor("#475b62"), // Green
        QColor("#536870"), // Yellow
        QColor("#708284"), // Blue
        QColor("#5956ba"), // Magenta
        QColor("#819090"), // Cyan
        QColor("#fcf4dc"), // White

        QColor("#708284"), // foreground
        QColor("#001e27"), // background
        QColor("#002831"), // Black
        QColor("#d11c24"), // Red
        QColor("#738a05"), // Green
        QColor("#a57706"), // Yellow
        QColor("#2176c7"), // Blue
        QColor("#c61c6f"), // Magenta
        QColor("#259286"), // Cyan
        QColor("#eae3cb"), // White
    };
    konsole->setColorTable(colorTable);

    layout2 = new QHBoxLayout;
    layout2->setMargin(0);
    layout2->addWidget(konsole);
    layout2->addWidget(scrollBar);

    layout = new QVBoxLayout;
    layout->setMargin(0);
    layout->addWidget(buttonAddData);
    layout->addWidget(buttonAddLink);
    layout->addWidget(buttonSearch);
    layout->addWidget(buttonScrollToEnd);
    layout->addLayout(layout2);

    QWidget* widget = new QWidget();
    widget->setLayout(layout);
    setCentralWidget(widget);

    QDesktopServices::setUrlHandler("file", this, "handleFileUrl");
}

void MainWindow::clickedAddData()
{
    bool ok;
    QString value = QInputDialog::getText(this, tr("QInputDialog::getText()"), tr("Text to add:"), QLineEdit::Normal, "", &ok);
    QByteArray raw = value.toUtf8();
    konsole->outputBlock(raw);
}

void MainWindow::clickedAddLink()
{
    QString url = QString("\r\n\x1b]8;;%1\x1b\\This is a link\x1b]8;;\x1b\\\r\n").arg(QUrl::fromLocalFile("C:\\notes.txt").toString());
    QByteArray rawurl = url.toUtf8();
    konsole->outputBlock(rawurl);
}

void MainWindow::clickedSearch()
{
    bool ok;
    QString text = QInputDialog::getText(this, tr("QInputDialog::getText()"), tr("Text to search:"), QLineEdit::Normal, "", &ok);
    konsole->search(text, false, false, false);
}

void MainWindow::clickedScrollToEnd()
{
    konsole->scrollToEnd();
}

void MainWindow::handleFileUrl(const QUrl &url)
{
    QMessageBox msgBox;
    msgBox.setText(url.toLocalFile());
    msgBox.exec();
}
