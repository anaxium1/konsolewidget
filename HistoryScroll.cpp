// Own
#include "HistoryScroll.h"

#include "HistoryType.h"

using namespace Konsole;

HistoryScroll::HistoryScroll(HistoryType *t) :
    _historyType(t)
{
}

HistoryScroll::~HistoryScroll() = default;

bool HistoryScroll::hasScroll() const
{
    return true;
}
