// Own
#include "TerminalColor.h"

// Qt
#include <QTimer>
#include <QPalette>
#include <QApplication>

namespace Konsole
{

    TerminalColor::TerminalColor(QObject *parent)
        : QObject(parent)
        , m_opacity(1.0)
        , m_blendColor(qRgba(0, 0, 0, 0xff))
        , m_cursorColor(QColor())
        , m_cursorTextColor(QColor())
    {
        setColorTable(m_defaultColorTable);
    }

    QColor TerminalColor::backgroundColor() const
    {
        return m_colorTable[DEFAULT_BACK_COLOR];
    }

    QColor TerminalColor::foregroundColor() const
    {
        return m_colorTable[DEFAULT_FORE_COLOR];
    }

    void TerminalColor::setColorTable(const QColor *table)
    {
        std::copy(table, table + TABLE_COLORS, m_colorTable);
        setBackgroundColor(m_colorTable[DEFAULT_BACK_COLOR]);
        onColorsChanged();
    }

    const QColor* TerminalColor::colorTable() const
    {
        return m_colorTable;
    }

    void TerminalColor::setOpacity(qreal opacity)
    {
        QColor color(m_blendColor);
        color.setAlphaF(opacity);
        m_opacity = opacity;

        m_blendColor = color.rgba();
        onColorsChanged();
    }

    void TerminalColor::visualBell()
    {
        swapFGBGColors();
        QTimer::singleShot(200, this, &TerminalColor::swapFGBGColors);
    }

    qreal TerminalColor::opacity() const
    {
        return m_opacity;
    }

    QRgb TerminalColor::blendColor() const
    {
        return m_blendColor;
    }

    void TerminalColor::setBackgroundColor(const QColor &color)
    {
        m_colorTable[DEFAULT_BACK_COLOR] = color;
        onColorsChanged();
    }

    void TerminalColor::setForegroundColor(const QColor &color)
    {
        m_colorTable[DEFAULT_FORE_COLOR] = color;
        onColorsChanged();
    }

    bool TerminalColor::event(QEvent *event)
    {
        switch (event->type()) {
            case QEvent::PaletteChange:
            case QEvent::ApplicationPaletteChange:
                onColorsChanged();
                break;

            default:
                break;
        }
        return QObject::event(event);
    }

    void TerminalColor::onColorsChanged()
    {
        QPalette palette = QApplication::palette();

        QColor buttonTextColor = m_colorTable[DEFAULT_FORE_COLOR];
        QColor backgroundColor = m_colorTable[DEFAULT_BACK_COLOR];
        backgroundColor.setAlphaF(m_opacity);

        QColor buttonColor = backgroundColor.toHsv();
        if (buttonColor.valueF() < 0.5) {
            buttonColor = buttonColor.lighter();
        } else {
            buttonColor = buttonColor.darker();
        }
        palette.setColor(QPalette::Button, buttonColor);
        palette.setColor(QPalette::Window, backgroundColor);
        palette.setColor(QPalette::Base, backgroundColor);
        palette.setColor(QPalette::WindowText, buttonTextColor);
        palette.setColor(QPalette::ButtonText, buttonTextColor);

        QWidget *widget = qobject_cast<QWidget*>(parent());

        widget->setPalette(palette);

        Q_EMIT onPalette(palette);

        widget->update();
    }

    void TerminalColor::swapFGBGColors()
    {
        QColor color = m_colorTable[DEFAULT_BACK_COLOR];
        m_colorTable[DEFAULT_BACK_COLOR] = m_colorTable[DEFAULT_FORE_COLOR];
        m_colorTable[DEFAULT_FORE_COLOR] = color;

        onColorsChanged();
    }

    const QColor TerminalColor::m_defaultColorTable[TABLE_COLORS] = {
        QColor(0x00, 0x00, 0x00), // Dfore
        QColor(0xFF, 0xFF, 0xFF), // Dback
        QColor(0x00, 0x00, 0x00), // Black
        QColor(0xB2, 0x18, 0x18), // Red
        QColor(0x18, 0xB2, 0x18), // Green
        QColor(0xB2, 0x68, 0x18), // Yellow
        QColor(0x18, 0x18, 0xB2), // Blue
        QColor(0xB2, 0x18, 0xB2), // Magenta
        QColor(0x18, 0xB2, 0xB2), // Cyan
        QColor(0xB2, 0xB2, 0xB2), // White
        // intensive versions
        QColor(0x00, 0x00, 0x00),
        QColor(0xFF, 0xFF, 0xFF),
        QColor(0x68, 0x68, 0x68),
        QColor(0xFF, 0x54, 0x54),
        QColor(0x54, 0xFF, 0x54),
        QColor(0xFF, 0xFF, 0x54),
        QColor(0x54, 0x54, 0xFF),
        QColor(0xFF, 0x54, 0xFF),
        QColor(0x54, 0xFF, 0xFF),
        QColor(0xFF, 0xFF, 0xFF),
        // Here are faint intensities, which may not be good.
        // faint versions
        QColor(0x00, 0x00, 0x00),
        QColor(0xFF, 0xFF, 0xFF),
        QColor(0x00, 0x00, 0x00),
        QColor(0x65, 0x00, 0x00),
        QColor(0x00, 0x65, 0x00),
        QColor(0x65, 0x5E, 0x00),
        QColor(0x00, 0x00, 0x65),
        QColor(0x65, 0x00, 0x65),
        QColor(0x00, 0x65, 0x65),
        QColor(0x65, 0x65, 0x65)
    };

}
