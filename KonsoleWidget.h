#ifndef KONSOLEWIDGET_H
#define KONSOLEWIDGET_H

#include <QtCore/qglobal.h>

#if defined(KONSOLEWIDGET_LIBRARY)
#  define KONSOLEWIDGET_EXPORT Q_DECL_EXPORT
#else
#  define KONSOLEWIDGET_EXPORT Q_DECL_IMPORT
#endif

#include <QWidget>
#include <QVBoxLayout>
#include <QByteArray>
#include <QScrollBar>

namespace Konsole {

    class TerminalDisplay;
    class Session;
    class EscapeSequenceUrlFilter;
    class RegExpFilter;
    class SearchHistoryTask;

    class KONSOLEWIDGET_EXPORT KonsoleWidget : public QWidget {
        Q_OBJECT

    public:
        KonsoleWidget(QWidget *parent = 0);

        void setTerminalFont(const QFont& font);
        void setColorTable(const QVector<QColor>& table);
        void setTextCodec(QTextCodec* codec);
        void setHistorySize(int lines);
        void setAllowedLinkSchema(const QStringList& allowedSchemas);
        void setReflowLines(bool enable);
        void setExternalScrollBar(QScrollBar* scrollBar);

    public slots:
        void outputBlock(const QByteArray& block);
        void clearSelection();
        void search(const QString& text, bool forwards, bool useRegex, bool matchCase);
        void scrollToEnd();

    protected:
        virtual void resizeEvent(QResizeEvent *);

    private slots:
        void configureRequest(const QPoint &position);

    private:
        QVBoxLayout* m_layout;
        TerminalDisplay* m_terminalDisplay;
        Session* m_session;
        EscapeSequenceUrlFilter* m_escapedUrlFilter;
        RegExpFilter* m_searchFilter;
        SearchHistoryTask* m_searchHistoryTask;
        QString m_lastSearchText;
        int m_searchStartLine;
    };

}

#endif
