#include "EscapeSequenceUrlFilterHotSpot.h"

#include <QApplication>
#include <QMouseEvent>
#include <QDebug>
#include <QDesktopServices>

#include "TerminalDisplay.h"

using namespace Konsole;

EscapeSequenceUrlHotSpot::EscapeSequenceUrlHotSpot(
    int startLine,
    int startColumn,
    int endLine,
    int endColumn,
    const QString &text,
    const QString &url) :
    HotSpot(startLine, startColumn, endLine, endColumn),
    _text(text),
    _url(url)
{
    setType(EscapedUrl);
}

void EscapeSequenceUrlHotSpot::activate(QObject *obj)
{
    Q_UNUSED(obj)

    QDesktopServices::openUrl(QUrl(_url));
}
