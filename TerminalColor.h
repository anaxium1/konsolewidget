#ifndef TERMINALCOLOR_HPP
#define TERMINALCOLOR_HPP

// Qt
#include <QWidget>
#include <QColor>

#include "CharacterColor.h"

namespace Konsole
{

    class Profile;
    class ColorScheme;

    class TerminalColor : public QObject
    {
        Q_OBJECT
    public:
        explicit TerminalColor(QObject *parent);

        QColor backgroundColor() const;
        QColor foregroundColor() const;
        void setColorTable(const QColor *table);
        const QColor *colorTable() const;

        void setOpacity(qreal opacity);

        void visualBell();

        qreal opacity() const;
        QRgb blendColor() const;

        QColor cursorColor() const
        {
            return m_cursorColor;
        }

        QColor cursorTextColor() const
        {
            return m_cursorTextColor;
        }

    public Q_SLOTS:
        void setBackgroundColor(const QColor &color);
        void setForegroundColor(const QColor &color);

    Q_SIGNALS:
        void onPalette(const QPalette &);

    protected:
        bool event(QEvent *event) override;
        void onColorsChanged();

    private Q_SLOTS:
        void swapFGBGColors();

    private:
        qreal m_opacity;
        QRgb m_blendColor;

        QColor m_cursorColor;
        QColor m_cursorTextColor;

        QColor m_colorTable[TABLE_COLORS];

        static const QColor m_defaultColorTable[];
    };
}

#endif
