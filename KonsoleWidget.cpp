#include <QVBoxLayout>
#include <QTextCodec>

#include "Session.h"
#include "TerminalDisplay.h"
#include "TerminalFonts.h"
#include "TerminalColor.h"
#include "TerminalScrollBar.h"
#include "Emulation.h"
#include "EscapeSequenceUrlFilter.h"
#include "FilterChain.h"
#include "SearchHistoryTask.h"
#include "RegExpFilter.h"

#include "KonsoleWidget.h"


using namespace Konsole;

KonsoleWidget::KonsoleWidget(QWidget *parent) :
    QWidget(parent),
    m_terminalDisplay(new TerminalDisplay(this)),
    m_session(new Session(this)),
    m_escapedUrlFilter(new EscapeSequenceUrlFilter(m_session, m_terminalDisplay)),
    m_searchFilter(new RegExpFilter()),
    m_searchHistoryTask(new SearchHistoryTask(this)),
    m_lastSearchText(""),
    m_searchStartLine(-1)
{
    connect(m_terminalDisplay, &TerminalDisplay::configureRequest, this, &KonsoleWidget::configureRequest);

    m_session->addView(m_terminalDisplay);

    m_searchHistoryTask->setAutoDelete(false);
    m_searchHistoryTask->addScreenWindow(m_session, m_terminalDisplay->screenWindow());

    m_terminalDisplay->filterChain()->addFilter(m_escapedUrlFilter);
    m_terminalDisplay->filterChain()->addFilter(m_searchFilter);

    m_layout = new QVBoxLayout();
    m_layout->setMargin(0);
    m_layout->addWidget(m_terminalDisplay);
    setLayout(m_layout);

    this->setFocus( Qt::OtherFocusReason );
    this->setFocusPolicy( Qt::WheelFocus );
    this->setFocusProxy(m_terminalDisplay);

    setTerminalFont(QFont("Consolas", 10));
    setTextCodec(QTextCodec::codecForName("UTF-8"));
    setHistorySize(1000);
}

void KonsoleWidget::setTerminalFont(const QFont& font)
{
    m_terminalDisplay->terminalFont()->setVTFont(font);
}

void KonsoleWidget::setColorTable(const QVector<QColor>& table)
{
    assert(table.size() == TABLE_COLORS);
    m_terminalDisplay->terminalColor()->setColorTable(table.constData());
}

void KonsoleWidget::setTextCodec(QTextCodec* codec)
{
    m_session->setCodec(codec);
}

void KonsoleWidget::setHistorySize(int lines)
{
    m_session->setHistorySize(lines);
}

void KonsoleWidget::setAllowedLinkSchema(const QStringList& allowedSchemas)
{
    m_terminalDisplay->setAllowedLinkSchema(allowedSchemas);
}

void KonsoleWidget::setReflowLines(bool enable)
{
    m_terminalDisplay->setReflowLines(enable);
}

void KonsoleWidget::setExternalScrollBar(QScrollBar* scrollBar)
{
    m_terminalDisplay->scrollBar()->setScrollBarPosition(Enum::ScrollBarHidden);
    connect(scrollBar, &QScrollBar::valueChanged, m_terminalDisplay->scrollBar(), &QScrollBar::setValue);
    connect(m_terminalDisplay->scrollBar(), &QScrollBar::rangeChanged, scrollBar, &QScrollBar::setRange);
    connect(m_terminalDisplay->scrollBar(), &QScrollBar::valueChanged, scrollBar, &QScrollBar::setValue);
}

void KonsoleWidget::outputBlock(const QByteArray& block)
{
    m_session->onReceiveBlock(block.constData(), block.length());
}

void KonsoleWidget::resizeEvent(QResizeEvent*)
{
    m_terminalDisplay->resize(this->size());
}

void KonsoleWidget::configureRequest(const QPoint &position)
{
    m_terminalDisplay->copyToClipboard();
    m_terminalDisplay->clearSelection();
}

void KonsoleWidget::clearSelection()
{
    m_terminalDisplay->clearSelection();
}

void KonsoleWidget::search(const QString& text, bool forwards, bool useRegex, bool matchCase)
{
    if (text != m_lastSearchText) {
        m_searchStartLine = -1;
    } else {
        m_searchStartLine = m_terminalDisplay->screenWindow()->currentResultLine();
    }

    if (!forwards && m_searchStartLine == -1) {
        m_searchStartLine = m_terminalDisplay->screenWindow()->lineCount();
    }

    m_lastSearchText = text;

    m_terminalDisplay->screenWindow()->setCurrentResultLine(-1);

    QRegularExpression regExp;
    if (useRegex) {
        regExp.setPattern(text);
    } else {
        regExp.setPattern(QRegularExpression::escape(text));
    }

    if (!matchCase) {
        regExp.setPatternOptions(QRegularExpression::CaseInsensitiveOption);
    }

    m_searchFilter->setRegExp(regExp);

    m_searchHistoryTask->setRegExp(regExp);
    m_searchHistoryTask->setSearchDirection(forwards ? Enum::ForwardsSearch : Enum::BackwardsSearch);
    m_searchHistoryTask->setStartLine(m_searchStartLine);
    m_searchHistoryTask->execute();

    m_terminalDisplay->processFilters();
    m_terminalDisplay->update();
}

void KonsoleWidget::scrollToEnd()
{
    m_terminalDisplay->screenWindow()->scrollTo(INT_MAX);
    m_terminalDisplay->screenWindow()->setTrackOutput(true);
    m_terminalDisplay->updateImage();
}
