#include "HistoryType.h"

using namespace Konsole;

HistoryType::HistoryType() = default;

HistoryType::~HistoryType() = default;

bool HistoryType::isUnlimited() const
{
    return maximumLineCount() == -1;
}
