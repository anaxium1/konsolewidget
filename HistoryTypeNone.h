#ifndef HISTORYTYPENONE_H
#define HISTORYTYPENONE_H

#include "HistoryType.h"

namespace Konsole
{

class HistoryTypeNone : public HistoryType
{
public:
    HistoryTypeNone();

    bool isEnabled() const override;
    int maximumLineCount() const override;

    void scroll(std::unique_ptr<HistoryScroll> &) const override;
};

}

#endif
