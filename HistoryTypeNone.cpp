#include "HistoryTypeNone.h"
#include "HistoryScrollNone.h"

using namespace Konsole;

HistoryTypeNone::HistoryTypeNone() = default;

bool HistoryTypeNone::isEnabled() const
{
    return false;
}

void HistoryTypeNone::scroll(std::unique_ptr<HistoryScroll> &old) const
{
    old = std::make_unique<HistoryScrollNone>();
}

int HistoryTypeNone::maximumLineCount() const
{
    return 0;
}
