// Own
#include "RandomizationRange.h"

// Qt
#include <QtGlobal>

namespace Konsole
{

    RandomizationRange::RandomizationRange()
        : hue(0.0)
        , saturation(0.0)
        , lightness(0.0)
    {}

    bool RandomizationRange::isNull() const
    {
        return qFuzzyIsNull(hue) && qFuzzyIsNull(saturation) && qFuzzyIsNull(lightness);
    }

}
