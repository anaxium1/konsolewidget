import os
from os.path import abspath, join
from sipbuild import Option
from pyqtbuild import PyQtBindings, PyQtProject
import PyQt5

class KonsoleWidgetProject(PyQtProject):
    def __init__(self):
        super().__init__()
        self.bindings_factories = [KonsoleWidgetBindings]

    def update(self, tool):
        """Allows SIP to find PyQt5 .sip files."""
        super().update(tool)
        self.sip_include_dirs.append(join(PyQt5.__path__[0], 'bindings'))

class KonsoleWidgetBindings(PyQtBindings):
    def __init__(self, project):
        super().__init__(project, name='KonsoleWidget',
                         sip_file='KonsoleWidget.sip',
                         qmake_QT=['widgets'])
